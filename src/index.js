var Ractive = require('ractive');
var sysPath = require('path');

var RactiveCompiler = function(config) {
    this.config = config;
}

RactiveCompiler.prototype = {
    brunchPlugin: true,
    type: 'template',
    extension: 'rac',
    pattern: /\.(?:rac|ractive|mustache)$/,

    include: [
        sysPath.join(__dirname, '..', 'node_modules', 'ractive', 'build', 'Ractive.js')
    ],

    compile: function(data, path, callback) {
        var error = null;
        var result = null;

        try {
            var holder = '(Ractive.templates = (Ractive.templates || {}))';
            var name = path.replace(/^app(\/|\\)templates(\/|\\)/, '')
            name = name.replace('\\', '/');
            name = name.replace(this.pattern, '');
            var data = JSON.stringify(Ractive.parse(data));
            result = holder + "['" + name + "'] = " + data + ';';
            return result;
        }
        catch(_error) {
            error = _error;
            return error;
        }
        finally {
            callback(error, result);
        }
    }
}

module.exports = RactiveCompiler;
