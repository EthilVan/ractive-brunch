ractive-brunch
==============

Adds [Ractive](http://ractivejs.org/) templates compilation support to
[brunch](http://brunch.io).

